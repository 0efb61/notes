---
title: Frequently Used Commands
author: Wilson Chang
time: 17:18
date: 25 October 2022
---

17:18 | 25 October 2022 | Wilson Chang

Personal commands that are used frequently.

## Handbrake-CLI

```
HandBrakeCLI -i input-file -o output-file -e x264 -q 31 -B 160 -v -f av_mkv --vfr
```

## Shred
```
shred -n 4 -v -z --remove *
```
