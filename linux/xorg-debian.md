---
title: Xorg on Debian
author: Wilson Chang
time: 17:00
date: 25 October 2022
---

17:00 | 25 October 2022 | Wilson Chang

Guidance on how to install xorg and xserver on Debian GNU/Linux.

## Code

```
sudo apt install xserver-xorg-video-intel xserver-xorg-core xserver-xorg xorg
```

