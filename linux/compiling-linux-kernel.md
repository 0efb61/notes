---
title: Compiling Linux Kernel
author: Wilson Chang
time: 17:28
date: 25 October 2022
---

17:28 | 25 October 2022 | Wilson Chang

Guidance on how to compile Linux Kernel.

## Code

```
# Install Prerequisites On Arch Linux
sudo pacman -S base-devel xmlto kmod inetutils bc libelf

# Configuration
make mrproper
make menuconfig (Do ur configuration stuff!!!)

# Compile
make
sudo make modules_install 
sudo cp -v arch/x86/boot/bzImage /boot/vmlinuz-YourKernelName
sudo mkinitcpio -k FullKernelName -c /etc/mkinitcpio.conf -g /boot/initramfs-YourKernelName.img
sudo cp System.map /boot/System.map-YourKernelName
```

