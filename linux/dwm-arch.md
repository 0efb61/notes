---
title: DWM on Arch
author: Wilson Chang
time: 17:41
date: 26 November 2022
---

17:41 | 26 November 2022 | Wilson Chang

Guidance on how to install DWM Window Manager on Arch Linux.

## Code

```
# Prerequisite
sudo pacman -Sy ttf-hack ttf-joypixels libx11 libxinerama libxft gst-libav gst-plugins-good xorg-server xorg-xinit

# Recommended Programs To Be Installed
sudo apt install lxappearance gpm papirus-icon-theme ranger feh p7zip unrar ffmpeg obs-studio pcmanfm adb fastboot
```

