---
title: "List of Flatpak Applications"
date: "2022-10-30"
author: "Wilson Chang"
description: "Personal list of Flatpak applications."
---

11:52 | 30 October 2022 | Wilson Chang

Personal list of Flatpak applications.

## Code

### Preferred Applications
```
flatpak install com.brave.Browser com.discordapp.Discord com.github.tchx84.Flatseal com.mattjakeman.ExtensionManager com.obsproject.Studio com.spotify.Client com.vscodium.codium io.github.cboxdoerfer.FSearch io.github.shiftey.Desktop org.chromium.Chromium org.gnome.Evince org.gnome.baobab org.gnome.gThumb org.mozilla.firefox md.obsidian.Obsidian im.riot.Riot
```

### Optional
```
flatpak install com.github.micahflee.torbrowser-launcher
flatpak install ca.desrt.dconf-editor
flatpak install com.anydesk.Anydesk
flatpak install com.github.qarmin.czkawka.Locale
flatpak install com.github.ransome1.sleek
flatpak install com.github.tenderowl.frog
flatpak install com.google.Chrome
flatpak install com.simplenote.Simplenote
flatpak install de.haeckerfelix.Fragments
flatpak install fr.handbrake.ghb
flatpak install io.elementary.Platform
flatpak install io.github.lainsce.Colorway
flatpak install io.github.peazip.PeaZip
flatpak install io.github.seadve.Mousai
flatpak install org.freefilesync.FreeFileSync
flatpak install org.gimp.GIMP
flatpak install org.gnome.clocks
flatpak install org.gnome.meld
flatpak install org.gtk.Gtk3theme.Pop-dark
flatpak install org.gustavoperedo.FontDownloader
flatpak install org.kde.kdenlive
flatpak install org.libreoffice.LibreOffice
flatpak install org.nickvision.tagger
flatpak install com.gitlab.newsflash
flatpak install com.notesnook.Notesnook
flatpak install io.gitlab.theevilskeleton.Upscaler
flatpak install io.github.diegoivan.pdf_metadata_editor
flatpak install me.iepure.devtoolbox
flatpak install org.nickvision.tubeconverter
flatpak install org.nickvision.money
flatpak install ir.imansalmani.IPlan
flatpak install io.github.mrvladus.List
flatpak install org.gnome.Todo
flatpak install org.gnome.gitlab.cheywood.Iotas
```