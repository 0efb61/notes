---
title: Upgrade To Fedora 37 From Fedora 36
author: Wilson Chang
time: 21:39
date: 27 October 2022
---

21:39 | 27 October 2022 | Wilson Chang

This guide shows you how to upgrade to Fedora 37 from Fedora 36 as Fedora 37 is now in Beta.

# Guide

### Update Fedora 36 System
```
sudo dnf upgrade --refresh
```
&nbsp;
### Remove Old Package
```
sudo dnf autoremove
```
&nbsp;
### Upgrade To Fedora 37
```
sudo dnf install dnf-plugin-system-upgrade -y
sudo dnf system-upgrade download --releasever=37
```
&nbsp;
>If you encounter any issues during this step, add `--alowerasing` to your command.
```
sudo dnf system-upgrade download --releasever=37 --allowerasing
```
**OR**
```
sudo dnf distro-sync
```

&nbsp;



### Reboot Your System To Complete Upgrade
```
sudo dnf system-upgrade reboot
```
&nbsp;
### Post Upgrade Cleanup
```
sudo dnf system-upgrade clean
```
&nbsp;
### Remove broken symlinks
```
sudo symlinks -r -d /usr
```
**OR**
```
sudo symlinks -r /usr | grep dangling
```
&nbsp;
&nbsp;
&nbsp;

# Reference

- https://www.linuxcapable.com/how-to-upgrade-fedora-36-to-fedora-37-with-gnome-43/
