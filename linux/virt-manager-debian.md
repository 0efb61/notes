---
title: Virt Manager on Debian
author: Wilson Chang
time: 17:15
date: 25 October 2022
---

17:15 | 25 October 2022 | Wilson Chang

Guidance on how to install QEMU & Virt Manager in Debian GNU/Linux.

## Code

```
sudo apt install qemu-kvm libvirt-clients libvirt-daemon libvirt-daemon-system bridge-utils virtinst virt-manager
sudo systemctl enable libvirtd
sudo systemctl start libvirtdThere are several shortcuts key that help speed your command typing!There are several shortcuts key that help speed your command typing!
sudo adduser $USER libvirt
sudo adduser $USER libvirt-qemu
```

## Reference Video
- https://www.youtube.com/watch?v=a7Bx4T5GvOs
