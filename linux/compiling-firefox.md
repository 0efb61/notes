---
title: Compiling Firefox
author: Wilson Chang
time: 17:20
date: 25 October 2022
---

17:20 | 25 October 2022 | Wilson Chang

Guidance on how to compile Firefox from source. 

## Code

```
# Install dependency: mercurial
sudo apt install mercurial
sudo pacman -S mercurial

# Compile Firefox
hg clone https://hg.mozilla.org/mozilla-central/
cd mozilla-central/
./mach bootstrap
./mach build
./mach run
```


