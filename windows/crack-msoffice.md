---
title: Crack Microsoft Office
author: Wilson Chang
time: 16:43
date: 25 October 2022
---

16:43 | 25 October 2022 | Wilson Chang

This guide shows you how to download and install Microsoft Office for free, and also activate it for free.

## Guide

1. Download the files from #Reference.
2. Extract the files.
3. Disable internet connection.
4. Run the executable file to install MS Office.
5. Enable internet connection.
6. Run the activator to activate MS Office.

## Reference

- https://mega.nz/folder/ZL5kgJyb#GLRhB8wclZhd1Ibfu7gzGw
