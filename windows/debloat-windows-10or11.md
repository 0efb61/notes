---
title: Debloat Windows 10 or 11
author: Wilson Chang
time: 16:31
date: 25 October 2022
---

16:31 | 25 October 2022 | Wilson Chang

This guide shows you how to debloat windows on Windows 10 or 11.

## Guide

1. Open `Windows Power Shell` as **admin**.
2. Paste the following command and run it.

### Code
```
iwr -useb https://christitus.com/win | iex
```

## Reference

This tool is created by Chris Titus Tech.

- https://youtu.be/tPRv-ATUBe4
- https://christitus.com/windows-tool/
