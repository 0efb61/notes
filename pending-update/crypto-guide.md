---
title: "Crypto Notes"
date: "2022-12-20"
author: "Wilson Chang"
description: "Some personal notes and guides related to crypto."
---

12:21 | 2022-12-20 | Wilson Chang

Some personal notes and guides related to crypto.

## Exchange Platform

- Coinbase
- Kraken
- Crypto.com
- Monero
- LocalMonero

## Wallet
- Cake Wallet
- Phantom
