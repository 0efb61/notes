---
title: "Hack Into Cyber"
date: "2022-11-06"
author: "Wilson Chang"
description: "Personal notes on getting into cyber."
---

16:16 | 2022-11-06 | Wilson Chang

Personal notes on getting into cyber.

## Visual learner vs audio learner? 
Ditch it! Connecting with people where they are and existing experiencing is the KEY!

## Linear approach vs non-linear approach?
Non-linear approach may be better than linear approach during learning.

## Boot camp are good?
Boot camp are short, and may be bad due to humans' learning process (Theory & Practice requires time).

## Should I sign up for college/university?
Depends. Access to teachers during university is essential. 

## Mechanism
- Curse of knowledge?
- Request for Projects -> Trial & Error -> Feedback

## Mental model 
Organize information

## OSI Model
Defense In Depth

## Resources
- VM Ware
- Kali Linux
- Parrot OS
- Nextron Aurora
- Security Onion
- Metasploitable 2
- Vulnhub
- Pfsense